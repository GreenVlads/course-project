﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Xceed.Words.NET;

namespace Caesar_s_cypher_project
{
    public partial class MainForm : Form
    {
        public static BindingList<string> text = new BindingList<string>();
        public static BindingList<string> modifiedText = new BindingList<string>();
        public static List<string> tempList = new List<string>();
 
        public static int shift; // сдвиг шифра

        public MainForm()
        {
            InitializeComponent();                     
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        public static BindingList<string> DeCypher(string[] stringValue, int shiftValue)
        {
            string RusLowLetters = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"; // 33
            string RusUpLetters = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
            BindingList<List<char>> tempList = new BindingList<List<char>>();
            int index = 0;
            foreach (string paragraph in stringValue)
            {
                tempList.Add(new List<char>());
                int letterIndex = 0;
                foreach (char letter in paragraph)
                {
                    if (RusUpLetters.Contains(stringValue[index][letterIndex]))
                    {
                        int alphabetPos = RusUpLetters.IndexOf(stringValue[index][letterIndex]);
                        if ((alphabetPos + shiftValue) > 32)
                        {
                            tempList[index].Add(RusUpLetters[alphabetPos + shiftValue - 33]);
                        }
                        else if ((alphabetPos + shiftValue) < 0)
                        {
                            tempList[index].Add(RusUpLetters[alphabetPos + shiftValue + 33]);
                        }
                        else
                        {
                            tempList[index].Add(RusUpLetters[alphabetPos + shiftValue]);
                        }
                    }
                    else if (RusLowLetters.Contains(stringValue[index][letterIndex]))
                    {
                        int alphabetPos = RusLowLetters.IndexOf(stringValue[index][letterIndex]);
                        if ((alphabetPos + shiftValue) > 32)
                        {
                            tempList[index].Add(RusLowLetters[alphabetPos + shiftValue - 33]);
                        }
                        else if ((alphabetPos + shiftValue) < 0)
                        {
                            tempList[index].Add(RusLowLetters[alphabetPos + shiftValue + 33]);
                        }
                        else
                        {
                            tempList[index].Add(RusLowLetters[alphabetPos + shiftValue]);
                        }
                    }
                    else
                    {
                        tempList[index].Add((char)(stringValue[index][letterIndex]));
                    }
                    letterIndex++;
                }
                index++;
            }

            BindingList<string> resultList = new BindingList<string>();
            foreach (List<char> paragraph in tempList)
            {
                string tempStr = "";
                foreach (char letter in paragraph)
                {
                    tempStr += letter;
                }
                resultList.Add(tempStr);
            }
            return resultList;
        }



        private void OpenFile_Click(object sender, EventArgs e)
        {
            try
            {
                tempList.Clear(); 
                text.Clear();
                if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                    return;
                if (Path.GetExtension(openFileDialog1.FileName) == ".docx")
                {
                    string filename = openFileDialog1.FileName;
                    using (DocX document = DocX.Load(filename))
                    {
                        List<Paragraph> list = document.Paragraphs.ToList();
                        foreach (Paragraph elem in list)
                        {
                            text.Add(elem.Text);
                        }
                    }
                }
                else
                {
                    string filename = openFileDialog1.FileName;
                    List<string> list = File.ReadAllLines(filename, Encoding.Default).ToList<string>();
                    foreach (string elem in list)
                    {
                        text.Add(elem);
                    }
                }
                textBox2.ReadOnly = true;
                textBox2.Text = string.Join(" ", text);
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void SaveFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox2.ReadOnly == true)
                {
                    if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                        return;
                    if (Path.GetExtension(openFileDialog1.FileName) == ".docx")
                    {
                        if (Path.GetExtension(saveFileDialog1.FileName) == ".docx")
                        {
                            string filename = saveFileDialog1.FileName;
                            using (DocX document = DocX.Create(filename))
                            {
                                foreach (string current in modifiedText)
                                {
                                    document.InsertParagraph(current);
                                }
                                document.Save();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Данный файл возможно сохранить только в формате .docx");
                        }
                    }
                    else
                    {
                        string filename = saveFileDialog1.FileName;
                        if (Path.GetExtension(saveFileDialog1.FileName) == ".txt")
                        {
                            File.WriteAllLines(filename, modifiedText, Encoding.Default);
                        }
                        else
                        {
                            MessageBox.Show("Данный файл возможно сохранить только в формате .txt");
                        }
                    }
                }
                else
                {
                    if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                        return;
                    if (Path.GetExtension(saveFileDialog1.FileName) == ".docx")
                    {
                        
                            string filename = saveFileDialog1.FileName;
                            using (DocX document = DocX.Create(filename))
                            {
                                foreach (string current in modifiedText)
                                {
                                    document.InsertParagraph(current);
                                }
                                document.Save();
                            }
                        
                        
                    }
                    else
                    {
                        string filename = saveFileDialog1.FileName;
                        
                            File.WriteAllLines(filename, modifiedText, Encoding.Default);
                       
                    }
                }
            }
            catch(Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            shift = (int)numericUpDown.Value;
        }

        private void ModifyText_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (textBox2.ReadOnly == false)
                {
                    text = new BindingList<string>(textBox2.Text.Split('\n').ToList<string>());
                    modifiedText = DeCypher(text.ToArray<string>(), shift);
                    textBox1.Text = string.Join(" ", DeCypher(textBox2.Text.Split(' '), shift));
                }
                else
                {
                    modifiedText = DeCypher(text.ToArray<string>(), shift);
                    textBox1.Text = string.Join(" ", modifiedText);
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void btnQuestion_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "Добро пожаловать в программу шифрования/дешифрования сообщений!\n\n" +
                "Для начала работы, вам необходимо открыть .docx или .txt файл с сообщением c помощью кнопки \"Файл->Открыть\".\n\n" +
                "Также вы можете ввести текст прямо в приложении. Для этого нажмите \"Файл->Ввести текст в окно\"\n\n" + 
                "Для шифрования/дешифрования сообщений вам необходимо указать шаг сдивига и обработать сообщение с помощью кнопки \"Обработать текст\".\n\n" + 
                "Сохранить сообщение в .docx файле вы можете с помощью кнопки \"Файл->Сохранить\"");
        }

        private void UsetText_Click(object sender, EventArgs e)
        {
            textBox2.ReadOnly = false;
            text.Clear();
            textBox2.Text = "";
        }

    }
}
