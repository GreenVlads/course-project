﻿using System;
using System.ComponentModel;
using Caesar_s_cypher_project;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MainFormTests
{
    [TestClass]
    public class MainFormTest
    {
        [TestMethod]
        public void DeCypher_AAAAand5()
        {
            // Arrange
            string[] inputStr = new string[1];
            inputStr[0] = "АААА";
            int inputShift = 5;
            BindingList<string> expected = new BindingList<string>();
            expected.Add("ЕЕЕЕ");

            // Act
            BindingList<string> actual = MainForm.DeCypher(inputStr, inputShift);

            // Assert
            Assert.AreEqual(expected[0], actual[0]);
        }

        [TestMethod]
        public void DeCypher_AaAaand5()
        {
            // Arrange
            string[] inputStr = new string[1];
            inputStr[0] = "АаАа";
            int inputShift = 5;
            BindingList<string> expected = new BindingList<string>();
            expected.Add("ЕеЕе");

            // Act
            BindingList<string> actual = MainForm.DeCypher(inputStr, inputShift);

            // Assert
            Assert.AreEqual(expected[0], actual[0]);
        }

        [TestMethod]
        public void DeCypher_qweertyand10()
        {
            // Arrange
            string[] inputStr = new string[1];
            inputStr[0] = "йцукен";
            int inputShift = 10;
            BindingList<string> expected = new BindingList<string>();
            expected.Add("уаэфоч");

            // Act
            BindingList<string> actual = MainForm.DeCypher(inputStr, inputShift);

            // Assert
            Assert.AreEqual(expected[0], actual[0]);
        }

        [TestMethod]
        public void DeCypher_voidand10()
        {
            // Arrange
            string[] inputStr = new string[1];
            inputStr[0] = "";
            int inputShift = 10;
            BindingList<string> expected = new BindingList<string>();
            expected.Add("");

            // Act
            BindingList<string> actual = MainForm.DeCypher(inputStr, inputShift);

            // Assert
            Assert.AreEqual(expected[0], actual[0]);
        }

        [TestMethod]
        public void DeCypher_signsand2()
        {
            // Arrange
            string[] inputStr = new string[1];
            inputStr[0] = "1А1а!А*а";
            int inputShift = 2;
            BindingList<string> expected = new BindingList<string>();
            expected.Add("1В1в!В*в");

            // Act
            BindingList<string> actual = MainForm.DeCypher(inputStr, inputShift);

            // Assert
            Assert.AreEqual(expected[0], actual[0]);
        }

        [TestMethod]
        public void DeCypher_Not_engand2()
        {
            // Arrange
            string[] inputStr = new string[1];
            inputStr[0] = "1A1a!A*a";   // english letters
            int inputShift = 2;
            BindingList<string> expected = new BindingList<string>();
            expected.Add("1В1в!В*в");

            // Act
            BindingList<string> actual = MainForm.DeCypher(inputStr, inputShift);

            // Assert
            Assert.AreNotEqual(expected[0], actual[0]);
        }
    }
}
